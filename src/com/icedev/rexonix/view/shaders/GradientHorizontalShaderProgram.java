package com.icedev.rexonix.view.shaders;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.shader.constants.ShaderProgramConstants;
import org.andengine.opengl.shader.exception.ShaderProgramLinkException;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.attribute.VertexBufferObjectAttributes;

import android.opengl.GLES20;

public class GradientHorizontalShaderProgram extends ShaderProgram {

	private static GradientHorizontalShaderProgram INSTANCE;

	public static int sUniformModelViewPositionMatrixLocation = ShaderProgramConstants.LOCATION_INVALID;
	
	public static final String VERTEXSHADER =
			"uniform mat4 " + ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX + ";\n" +
			"attribute vec3 " + ShaderProgramConstants.ATTRIBUTE_POSITION + ";             \n" +
			"varying vec3 v_vertex;                                                        \n" +
			"void main()                                                                   \n" +
			"{                                                                             \n" +
			"   v_vertex=" + ShaderProgramConstants.ATTRIBUTE_POSITION + ";                \n" +
			"   gl_Position = " + ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX + " * vec4(" + ShaderProgramConstants.ATTRIBUTE_POSITION + ", 1.0);\n" +
			"}";
	
	public static final String FRAGMENTSHADER =
			"precision mediump float;                                      \n" +
			"varying vec3 v_vertex;                                        \n" +
			"void main()                                                   \n" +
			"{                                                             \n" +
			"   gl_FragColor = vec4(0.1, 0.1, 0.5, v_vertex/100.0);        \n" +
			"}";

	private GradientHorizontalShaderProgram() {
		super(GradientHorizontalShaderProgram.VERTEXSHADER,
				GradientHorizontalShaderProgram.FRAGMENTSHADER);
	}

	public static GradientHorizontalShaderProgram getInstance() {
		if (GradientHorizontalShaderProgram.INSTANCE == null) {
			GradientHorizontalShaderProgram.INSTANCE = new GradientHorizontalShaderProgram();
		}
		return GradientHorizontalShaderProgram.INSTANCE;
	}

	@Override
	protected void link(final GLState pGLState)
			throws ShaderProgramLinkException {
		GLES20.glBindAttribLocation(this.mProgramID, ShaderProgramConstants.ATTRIBUTE_POSITION_LOCATION, ShaderProgramConstants.ATTRIBUTE_POSITION); 

		super.link(pGLState);

 		GradientHorizontalShaderProgram.sUniformModelViewPositionMatrixLocation = this.getUniformLocation(ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX);
	}

	@Override
	public void bind(final GLState pGLState,
			final VertexBufferObjectAttributes pVertexBufferObjectAttributes) {
		super.bind(pGLState, pVertexBufferObjectAttributes);
		GLES20.glUniformMatrix4fv(GradientHorizontalShaderProgram.sUniformModelViewPositionMatrixLocation, 1, false, pGLState.getModelViewProjectionGLMatrix(), 0);
	}
	
	@Override
	public void unbind(final GLState pGLState) {
		super.unbind(pGLState);
	}

}
