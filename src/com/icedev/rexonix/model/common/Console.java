package com.icedev.rexonix.model.common;

/*import java.awt.Color;
import java.awt.Graphics;*/


public class Console {

	/*private World world;

	private Object serialized;

	private LevelProvider levelProvider;*/

	//private ModelContext context;

	int width;

	int height;

	int panelSize = 18;

	public Console() {
		//context = new ModelContext(MainActivity.CAMERA_WIDTH, MainActivity.CAMERA_HEIGHT);
		//levelProvider = new T1LevelProvider(context);
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	/*public void paint(Graphics g) {
		g.setColor(new Color(0, 0, 0));
		g.fillRect(0, 0, width, height);

		if (world == null) {
			g.setColor(new Color(100, 100, 100));
			g.drawString("Press 'r' to start new game!", 20, 20);
		} else {
			if (world.isFinished()) {
				g.setColor(new Color(100, 100, 100));
				g.drawString("Finished", 20, 20);
			} else if (world.isGaveOver()) {
				g.setColor(new Color(100, 100, 100));
				g.drawString("Game over!", 20, 20);
			} else if (!world.isProcessed()) {
				paintGame(g);
				g.setColor(new Color(100, 100, 100));
				g.drawString("Paused", 20, 20);
			} else {
				paintGame(g);
			}
		}
	}

	public void paintGame(Graphics g) {
		int worldWidth = context.getWidth();
		int worldHeight = context.getHeight();
		double kX = ((double) width) / ((double) worldWidth);
		double kY = ((double) height - panelSize) / ((double) worldHeight);

		kX = 1;
		kY = 1;

		g.setColor(new Color(200, 200, 200));
		int[][] cells = world.getEarth().getCells();
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				if (cells[i][j] == Layer.FILLED) {
					g.fillRect(((int) (((double) i) * kX)),
							((int) (((double) j) * kY)), ((int) kX), ((int) kY));
				}
			}
		}

		g.setColor(new Color(0, 200, 200));
		cells = world.getPath().getCells();
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				if (cells[i][j] == Layer.FILLED) {
					g.fillRect(((int) (((double) i) * kX)),
							((int) (((double) j) * kY)), ((int) kX), ((int) kY));
				}
			}
		}

		g.setColor(new Color(100, 100, 200));
		for (Alien alien : world.getAliens()) {
			g.fillRect((int) (((double) alien.getX()) * kX),
					(int) (((double) alien.getY()) * kY),
					(int) (((double) alien.getWidth()) * kX),
					(int) (((double) alien.getHeight()) * kY));
		}

		g.setColor(new Color(200, 0, 200));
		Player player = world.getPlayer();
		g.fillRect((int) (((double) player.getX()) * kX),
				(int) (((double) player.getY()) * kY),
				(int) (((double) player.getWidth()) * kX),
				(int) (((double) player.getHeight()) * kY));

		g.setColor(new Color(100, 100, 100));
		g.drawString(
				"Lifes: " + world.getLifeCount() + ", Level: "
						+ world.getLevelNumber(), 10, height - 4);
	}

	public void pause() {
		if (world != null)
			world.stop();
	}

	public void cont() {
		if (world != null)
			world.cont();
	}

	public void down() {
		if (world != null)
			world.down();
	}

	public void up() {
		if (world != null)
			world.up();
	}

	public void left() {
		if (world != null)
			world.left();
	}

	public void right() {
		if (world != null)
			world.right();
	}

	public void tick() {
		if (world != null) {
			world.calculate();
			world.apply();
		}
	}

	public void save() {
		if (world != null && !world.isGaveOver()) {
			world.stop();
			serialized = world.serialize();
		}
	}

	public void load() {
		if (serialized != null) {
			world = World.deserialize(serialized);
			world.setContext(context);
			world.setLevelProvider(levelProvider);
			world.cont();
		}
	}

	public void startNewGame() {
		world = new World(context, levelProvider);
		world.start();
	}*/

}
