package com.icedev.rexonix.model.actor;

import com.icedev.rexonix.model.IModelContext;

public class Player extends Actor {

	private static final long serialVersionUID = 1L;

	public final static String PLAYER = "Player";

	private int savedBeforeExpandX;

	private int savedBeforeExpandY;

	private int savedBeforeExpandWidth;

	private int savedBeforeExpandHeight;

	private int prevX;

	private int prevY;

	private int prevWidth;

	private int prevHeight;

	private int lastActualX;

	private int lastActualY;

	private int lastActualWidth;

	private int lastActualHeight;

	public Player(IModelContext context, int x, int y, int width, int height,
			int speedX, int speedY) {
		super(context, PLAYER, x, y, width, height, speedX, speedY);
		savePrevState();
	}

	public void savePrevState() {
		prevX = getX();
		prevY = getY();
		prevWidth = getWidth();
		prevHeight = getHeight();
	}

	public void saveLastActualState() {
		lastActualX = getPrevX();
		lastActualY = getPrevY();
		lastActualWidth = getPrevWidth();
		lastActualHeight = getPrevHeight();
	}

	public void restoreLastActualState() {
		setX(lastActualX);
		setY(lastActualY);
		setWidth(lastActualWidth);
		setHeight(lastActualHeight);
	}

	public int getPrevCenterX() {
		return getPrevX() + getWidth() / 2;
	}

	public int getPrevCenterY() {
		return getPrevY() + getHeight() / 2;
	}

	public int getPrevX() {
		return prevX;
	}

	public int getPrevY() {
		return prevY;
	}

	public int getPrevWidth() {
		return prevWidth;
	}

	public int getPrevHeight() {
		return prevHeight;
	}

	public int getLastActualX() {
		return lastActualX;
	}

	public int getLastActualY() {
		return lastActualY;
	}

	public int getLastActualWidth() {
		return lastActualWidth;
	}

	public int getLastActualHeight() {
		return lastActualHeight;
	}

	public void expandSingle() {
		saveBeforeExpand();
		expandBounds(getSpeedX() < 0 ? 1 : 0, getSpeedX() > 0 ? 1 : 0,
				getSpeedY() < 0 ? 1 : 0, getSpeedY() > 0 ? 1 : 0);
		shrinkByBounds();
	}

	private void shrinkByBounds() {
		if (getX() < 0)
			setX(0);
		if (getY() < 0)
			setY(0);
		if (getX() + getWidth() > getContext().getWidth())
			setWidth(getWidth()
					- (getX() + getWidth() - getContext().getWidth()));
		if (getY() + getHeight() > getContext().getHeight())
			setHeight(getHeight()
					- (getY() + getHeight() - getContext().getHeight()));
	}

	public void expandBounds(int left, int right, int top, int bottom) {
		setX(getX() - left);
		setWidth(getWidth() + left + right);
		setY(getY() - top);
		setHeight(getHeight() + top + bottom);
	}

	public void restoreBeforeExpand() {
		setX(savedBeforeExpandX);
		setY(savedBeforeExpandY);
		setWidth(savedBeforeExpandWidth);
		setHeight(savedBeforeExpandHeight);
	}

	private void saveBeforeExpand() {
		savedBeforeExpandX = getX();
		savedBeforeExpandY = getY();
		savedBeforeExpandWidth = getWidth();
		savedBeforeExpandHeight = getHeight();
	}

	@Override
	public void down() {
		setSpeed(0, 1);
	}

	@Override
	public void up() {
		setSpeed(0, -1);
	}

	@Override
	public void left() {
		setSpeed(-1, 0);
	}

	@Override
	public void right() {
		setSpeed(1, 0);
	}

}
