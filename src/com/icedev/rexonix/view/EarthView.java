package com.icedev.rexonix.view;

import java.io.IOException;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.util.GLState;
import org.andengine.ui.activity.BaseGameActivity;

import com.icedev.rexonix.model.layer.Earth;
import com.icedev.rexonix.view.textures.DynamicAlphaControlledTexture;

public class EarthView extends AbstractView {

	private Sprite sprite;

	private ITexture texture;

	public EarthView(BaseGameActivity activity, Earth model) {
		super(activity, model);
		setColor(1, 1, 0);
		update();
	}

	@Override
	public Earth getModel() {
		return (Earth) super.getModel();
	}

	@Override
	public void update() {
		int[][] cells = getModel().getCells();
		if (sprite == null) {
			try {
				texture = new DynamicAlphaControlledTexture(getActivity()
						.getTextureManager(), getModel().getCells());
				ITextureRegion textureRegion = TextureRegionFactory
						.extractFromTexture(texture);
				texture.load();
				sprite = new Sprite(getX(), getY(), cells.length,
						cells[0].length, textureRegion, getActivity()
								.getVertexBufferObjectManager());
				attachChild(sprite);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected void draw(final GLState pGLState, final Camera pCamera) {
		if (texture != null && getModel().isUpdated()) {
			try {
				texture.reloadToHardware(pGLState);
			} catch (IOException e) {
				e.printStackTrace();
			}
			getModel().alreadyUpdated();
		}
	}

}
