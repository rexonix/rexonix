package com.icedev.rexonix.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	private IModel parent;

	private transient IModelContext context;

	private String name;

	List<IModel> childs = new ArrayList<IModel>();

	Map<String, IModel> nameToChild = new HashMap<String, IModel>();

	public AbstractModel(IModelContext context, String name) {
		this.context = context;
		this.name = name;
	}

	@Override
	public IModelContext getContext() {
		return context;
	}

	@Override
	public void calculate() {
		for (IModel model : childs) {
			model.calculate();
		}
		calculateThis();
	}

	@Override
	public void apply() {
		for (IModel model : childs) {
			model.apply();
		}
		applyThis();
	}

	@Override
	public void setParent(IModel parent) {
		this.parent = parent;
	}

	@Override
	public void removeChild(IModel model) {
		childs.remove(model);
		nameToChild.remove(model.getName());
	}

	@Override
	public void addChild(IModel model) {
		childs.add(model);
		nameToChild.put(model.getName(), model);
		model.setParent(this);
	}

	@Override
	public IModel getParent() {
		return parent;
	}

	@Override
	public void down() {
		for (IModel model : childs) {
			model.down();
		}
	}

	@Override
	public void up() {
		for (IModel model : childs) {
			model.up();
		}
	}

	@Override
	public void left() {
		for (IModel model : childs) {
			model.left();
		}
	}

	@Override
	public void right() {
		for (IModel model : childs) {
			model.right();
		}
	}

	@Override
	public IModel getModel(String name) {
		IModel model = nameToChild.get(name);
		if (model == null) {
			for (IModel child : childs) {
				model = child.getModel(name);
				if (model != null) {
					return model;
				}
			}
		}
		return model;
	}

	@Override
	public void setContext(IModelContext context) {
		this.context = context;
		for (IModel child : childs) {
			child.setContext(context);
		}
	}

	public List<IModel> getChilds() {
		return childs;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void removeChilds() {
		childs.clear();
		nameToChild.clear();
	}

	public void calculateThis() {
	}

	public void applyThis() {
	}

	@Override
	public String toString() {
		return name;
	}

}
