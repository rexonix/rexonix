package com.icedev.rexonix.model.layer;

import com.icedev.rexonix.model.AbstractModel;
import com.icedev.rexonix.model.IModelContext;

public class Layer extends AbstractModel {

	private static final long serialVersionUID = 1L;

	public final static int EMPTY = 0;

	public final static int FILLED = 1;

	private int cells[][];

	public Layer(IModelContext context, String name) {
		super(context, name);
		initialize();
	}

	private void initialize() {
		IModelContext context = getContext();
		if (context != null && cells == null) {
			cells = new int[context.getWidth()][context.getHeight()];
			clean();
		}
	}

	@Override
	public void setContext(IModelContext context) {
		super.setContext(context);
		initialize();
	}

	public boolean isIntersectFilled(int x, int y) {
		return cells[x][y] == FILLED;
	}

	public boolean isIntersectEmpty(int x, int y, int width, int height) {
		for (int i = x; i < x + width; i++) {
			for (int j = y; j < y + height; j++) {
				if (cells[i][j] == EMPTY)
					return true;
			}
		}
		return false;
	}

	public boolean isIntersectFilled(int x, int y, int width, int height) {
		for (int i = x; i < x + width; i++) {
			for (int j = y; j < y + height; j++) {
				if (cells[i][j] == FILLED)
					return true;
			}
		}
		return false;
	}

	public void clean() {
		fillEmpty(0, 0, getContext().getWidth(), getContext().getHeight());
	}

	public void fillEmpty(int x, int y, int width, int height) {
		fill(x, y, width, height, EMPTY);
	}

	public void fillMarker(int x, int y, int width, int height) {
		fill(x, y, width, height, FILLED);
	}

	public void fill(int x, int y, int width, int height, int value) {
		for (int i = x; i < x + width; i++)
			for (int j = y; j < y + height; j++)
				cells[i][j] = value;
	}

	public int[][] getCells() {
		return cells;
	}

}
