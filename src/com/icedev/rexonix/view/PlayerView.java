package com.icedev.rexonix.view;

import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import com.icedev.rexonix.model.actor.Player;

public class PlayerView extends ActorView {

	public PlayerView(BaseGameActivity activity, Player player) {
		super(activity, player, new Color(0.5f, 0.8f, 0.5f));
	}

	@Override
	public Player getModel() {
		return (Player) super.getModel();
	}

}
