package com.icedev.rexonix;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import com.icedev.rexonix.common.EntityFactory;
import com.icedev.rexonix.model.IModel;
import com.icedev.rexonix.model.ModelContext;
import com.icedev.rexonix.model.World;
import com.icedev.rexonix.model.actor.Player;
import com.icedev.rexonix.model.common.LevelProvider;
import com.icedev.rexonix.model.t1.T1LevelProvider;
import com.icedev.rexonix.view.AbstractView;

public class MainActivity extends SimpleBaseGameActivity {

	private ModelContext context;

	private World world;

	private LevelProvider levelProvider;

	private EntityFactory entityFactory;

	private Camera camera;

	private Scene scene;

	public static final int CAMERA_WIDTH = 200;

	public static final int CAMERA_HEIGHT = 400;

	@Override
	public EngineOptions onCreateEngineOptions() {
		context = new ModelContext(CAMERA_WIDTH, CAMERA_HEIGHT);
		levelProvider = new T1LevelProvider(context);
		world = new World(context, levelProvider);
		entityFactory = new EntityFactory();

		camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.PORTRAIT_FIXED, new FillResolutionPolicy(),
				camera);
		return engineOptions;
	}

	@Override
	protected void onCreateResources() {
	}

	@Override
	protected Scene onCreateScene() {
		initializeView();
		registerUpdate();
		registerControls();
		return scene;
	}

	protected void registerControls() {
		scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {
				Player player = world.getPlayer();
				float deltaX = player.getX() - pSceneTouchEvent.getX();
				float deltaY = player.getY() - pSceneTouchEvent.getY();
				if (Math.abs(deltaX) > Math.abs(deltaY)) {
					player.setSpeed(deltaX < 0 ? 1 : -1, 0);
				} else {
					player.setSpeed(0, deltaY < 0 ? 1 : -1);
				}
				return false;
			}
		});
	}

	protected void registerUpdate() {
		scene.registerUpdateHandler(new TimerHandler(0.01f, true,
				new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						step();
					}
				}));
	}

	protected void initializeView() {
		scene = new Scene();
		scene.setBackground(new Background(0.0f, 0.0f, 0.0f));
		world.start();
		for (IModel model : world.getChilds()) {
			Entity entity = entityFactory.create(this, model);
			if (entity != null) {
				scene.attachChild(entity);
			}
		}
	}

	protected void step() {
		world.calculate();
		for (int i = 0; i < scene.getChildCount(); i++) {
			IEntity entity = scene.getChildByIndex(i);
			if (entity instanceof AbstractView) {
				((AbstractView) entity).update();
			}
		}
	}
}
