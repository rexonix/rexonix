package com.icedev.rexonix.model.actor;

import com.icedev.rexonix.model.IModelContext;

public class Alien extends Actor {

	private static final long serialVersionUID = 1L;

	public Alien(IModelContext context, String name, int x, int y, int width,
			int height, int speedX, int speedY) {
		super(context, name, x, y, width, height, speedX, speedY);
	}

}
