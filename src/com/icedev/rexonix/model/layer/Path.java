package com.icedev.rexonix.model.layer;

import java.util.ArrayList;
import java.util.List;

import com.icedev.rexonix.model.IModelContext;

public class Path extends Layer {

	private static final long serialVersionUID = 1L;

	public final static String PATH = "Path";

	private boolean isActive = false;

	private List<int[]> points;

	public Path(IModelContext context) {
		super(context, PATH);
	}

	public boolean isActive() {
		return isActive;
	}

	public void add(int lastX, int lastY) {
		List<int[]> points = getPoints();
		if (points.size() < 2) {
			points.add(new int[] { lastX, lastY });
		} else {
			int[] prevXY = points.get(points.size() - 1);
			int[] prevPrevXY = points.get(points.size() - 2);
			if (prevXY[0] == prevPrevXY[0] && prevXY[0] == lastX) {
				prevXY[1] = lastY;
			} else if (prevXY[1] == prevPrevXY[1] && prevXY[1] == lastY) {
				prevXY[0] = lastX;
			} else {
				points.add(new int[] { lastX, lastY });
			}
		}
	}

	public void activate(int startX, int startY) {
		isActive = true;
		add(startX, startY);
	}

	public void deactivate() {
		isActive = false;
	}

	public List<int[]> getPoints() {
		if (points == null)
			points = new ArrayList<int[]>();
		return points;
	}

	@Override
	public void clean() {
		super.clean();
		getPoints().clear();
	}

}
