package com.icedev.rexonix.view;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import com.icedev.rexonix.model.actor.Actor;

public abstract class ActorView extends AbstractView {

	private Rectangle mRectangle;

	public ActorView(BaseGameActivity activity, Actor actor, Color color) {
		super(activity, actor);
		setColor(color);
		update();
	}

	@Override
	public Actor getModel() {
		return (Actor) super.getModel();
	}

	@Override
	public void update() {
		Actor actor = getModel();
		if (mRectangle == null) {
			mRectangle = new Rectangle(0, 0, actor.getWidth(),
					actor.getHeight(), getActivity()
							.getVertexBufferObjectManager());
			mRectangle.setColor(getColor());
			attachChild(mRectangle);
		} else {
			mRectangle.setSize(actor.getWidth(), actor.getHeight());
		}
		setPosition(actor.getX(), actor.getY());
	}

	protected Rectangle getRectangle() {
		return mRectangle;
	}

}
