package com.icedev.rexonix.view.textures;

import java.io.IOException;

import org.andengine.opengl.texture.TextureManager;

public class DynamicAlphaControlledTexture extends AbstractControlledTexture {

	private int[][] cells;

	private int[][][] textureArray;

	public DynamicAlphaControlledTexture(final TextureManager pTextureManager,
			int[][] cells) throws IOException {
		super(pTextureManager);
		this.cells = cells;
	}

	@Override
	public int getWidth() {
		return cells.length;
	}

	@Override
	public int getHeight() {
		return cells[0].length;
	}

	@Override
	protected int[][][] getTextureArray() {
		if (textureArray == null) {
			textureArray = new int[getWidth()][getHeight()][bytesPerRGBAPixel];
			for (int i = 0; i < getWidth(); i++) {
				for (int j = 0; j < getHeight(); j++) {
					textureArray[i][j][0] = 100;
					textureArray[i][j][1] = 100;
					textureArray[i][j][2] = 100;
				}
			}
		}
		updateTextureArray();
		return textureArray;
	}

	protected void updateTextureArray() {
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				textureArray[i][j][3] = cells[i][j] == 0 ? 0 : 180;
			}
		}
	}
}
