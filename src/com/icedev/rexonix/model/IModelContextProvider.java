package com.icedev.rexonix.model;

public interface IModelContextProvider {

	public IModelContext getContext();

}
