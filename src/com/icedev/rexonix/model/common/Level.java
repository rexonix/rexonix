package com.icedev.rexonix.model.common;

import com.icedev.rexonix.model.World;

public abstract class Level {

	abstract public void initializeWorld(World world);
	
	abstract public boolean isFinished(World world);

}
