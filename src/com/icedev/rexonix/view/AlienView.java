package com.icedev.rexonix.view;

import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import com.icedev.rexonix.model.actor.Alien;

public class AlienView extends ActorView {

	public AlienView(BaseGameActivity activity, Alien alien) {
		super(activity, alien, new Color(0.8f, 0.5f, 0.5f));
	}

	@Override
	public Alien getModel() {
		return (Alien) super.getModel();
	}

}
