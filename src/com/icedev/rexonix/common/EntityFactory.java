package com.icedev.rexonix.common;

import org.andengine.entity.Entity;
import org.andengine.ui.activity.BaseGameActivity;

import com.icedev.rexonix.model.IModel;
import com.icedev.rexonix.model.actor.Alien;
import com.icedev.rexonix.model.actor.Player;
import com.icedev.rexonix.model.layer.Earth;
import com.icedev.rexonix.model.layer.Path;
import com.icedev.rexonix.view.AlienView;
import com.icedev.rexonix.view.EarthView;
import com.icedev.rexonix.view.PathView;
import com.icedev.rexonix.view.PlayerView;

public class EntityFactory {

	public Entity create(BaseGameActivity activity, IModel model) {
		if (model instanceof Player) {
			return new PlayerView(activity, (Player) model);
		} else if (model instanceof Alien) {
			return new AlienView(activity, (Alien) model);
		} else if (model instanceof Earth) {
			return new EarthView(activity, (Earth) model);
		} else if (model instanceof Path) {
			return new PathView(activity, (Path) model);
		}
		return null;
	}

}
