package com.icedev.rexonix.view.common;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class WideLine extends Rectangle {

	private float x1, x2, y1, y2;

	public WideLine(float x1, float y1, float x2, float y2, float width,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		super(x2 - x1 == 0 ? x1 - width / 2 : x1 + width / 2, y2 - y1 == 0 ? y1
				- width / 2 : y1 + width / 2, x2 - x1 == 0 ? width : x2 - x1,
				y2 - y1 == 0 ? width : y2 - y1, pVertexBufferObjectManager);
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public float getX1() {
		return x1;
	}

	public float getX2() {
		return x2;
	}

	public float getY1() {
		return y1;
	}

	public float getY2() {
		return y2;
	}

}
