package com.icedev.rexonix.view.textures;

import java.io.IOException;

import org.andengine.opengl.texture.TextureManager;

public class SampleStaticControlledTexture extends AbstractControlledTexture {

	private int width;

	private int height;

	public SampleStaticControlledTexture(final TextureManager pTextureManager)
			throws IOException {
		super(pTextureManager);
		this.width = 3;
		this.height = 3;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	protected int[][][] getTextureArray() {
		return new int[][][] {
				{ { 200, 0, 0, 150 }, { 0, 200, 0, 150 }, { 0, 0, 200, 150 } },
				{ { 200, 0, 0, 100 }, { 0, 200, 0, 100 }, { 0, 0, 200, 100 } } };
	}
}
