package com.icedev.rexonix.view.textures;

import java.io.IOException;

import org.andengine.opengl.texture.PixelFormat;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.util.GLState;

import com.icedev.rexonix.view.common.GLUtils;

public abstract class AbstractControlledTexture extends Texture {

	public final static int bytesPerRGBAPixel = 4;

	private int textureId;

	public AbstractControlledTexture(final TextureManager pTextureManager)
			throws IOException {
		super(pTextureManager, PixelFormat.RGBA_4444, TextureOptions.DEFAULT,
				null);
	}

	@Override
	protected void writeTextureToHardware(GLState pGLState) throws IOException {
		textureId = GLUtils.generateTexture(getTextureArray());
	}

	public int getTextureId() {
		return textureId;
	}

	abstract protected int[][][] getTextureArray();

}
