package com.icedev.rexonix.model;

public interface IModelContext {

	public int getWidth();

	public int getHeight();

}
