package com.icedev.rexonix.view;

import java.util.ArrayList;
import java.util.List;

import org.andengine.ui.activity.BaseGameActivity;

import com.icedev.rexonix.model.World;
import com.icedev.rexonix.model.layer.Path;
import com.icedev.rexonix.view.common.WideLine;

public class PathView extends AbstractView {

	private List<WideLine> lines = new ArrayList<WideLine>();

	public PathView(BaseGameActivity activity, Path model) {
		super(activity, model);
		setColor(1, 1, 0);
		update();
	}

	@Override
	public Path getModel() {
		return (Path) super.getModel();
	}

	private WideLine createWideLine(float x1, float x2, float y1, float y2) {
		float width = ((World) getModel().getParent()).getPlayer().getWidth();
		WideLine line = new WideLine(x1, y1, x2, y2, width, getActivity()
				.getVertexBufferObjectManager());
		line.setColor(getColor());
		return line;
	}

	@Override
	public void update() {
		if (getModel().isActive()) {
			if (lines.size() > 0) {
				WideLine line = lines.get(lines.size() - 1);
				List<int[]> list = getModel().getPoints();
				int[] curPoint = list.get(list.size() - 1);
				if (line.getX1() == line.getX2() && line.getX2() != curPoint[0]) {
					WideLine newLine = createWideLine(line.getX2(),
							curPoint[0], line.getY2(), curPoint[1]);
					lines.add(newLine);
					attachChild(newLine);
				} else if (line.getY1() == line.getY2()
						&& line.getY2() != curPoint[1]) {
					WideLine newLine = createWideLine(line.getX2(),
							curPoint[0], line.getY2(), curPoint[1]);
					lines.add(newLine);
					attachChild(newLine);
				} else {
					detachChild(line);
					lines.remove(lines.size() - 1);
					WideLine newLine = createWideLine(line.getX1(),
							curPoint[0], line.getY1(), curPoint[1]);
					lines.add(newLine);
					attachChild(newLine);
				}
			} else {
				List<int[]> list = getModel().getPoints();
				if (list.size() >= 2) {
					int[] startPoint = list.get(0);
					int[] curPoint = list.get(1);
					WideLine line = createWideLine(startPoint[0], curPoint[0],
							startPoint[1], curPoint[1]);
					lines.add(line);
					attachChild(line);
				}
			}
		} else {
			if (lines.size() > 0) {
				for (WideLine line : lines) {
					detachChild(line);
				}
				lines.clear();
			}
		}
	}

}
