package com.icedev.rexonix.model.layer;

import com.icedev.rexonix.model.IModelContext;

public class FillHelper extends Layer {

	private static final long serialVersionUID = 1L;

	public static final String FILL_HELPER = "Fill helper";

	public FillHelper(IModelContext context) {
		super(context, FILL_HELPER);
	}

}
