package com.icedev.rexonix.model.layer;

import com.icedev.rexonix.model.IModelContext;

public class Earth extends Layer {

	private static final long serialVersionUID = 1L;

	public final static String EARTH = "Earth";

	private double areaValue;

	private boolean updated = false;

	public Earth(IModelContext context) {
		super(context, EARTH);
	}

	public double areaValue() {
		return areaValue;
	}

	public void updateAreaValue() {
		int counter = 0;
		int[][] cells = getCells();
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				if (cells[i][j] != EMPTY)
					counter++;
			}
		}
		areaValue = ((double) counter)
				/ ((double) (getContext().getWidth() * getContext().getHeight()));
	}

	public void alreadyUpdated() {
		updated = false;
	}

	public boolean isUpdated() {
		return updated;
	}

	public void updated() {
		updated = true;
	}

}
