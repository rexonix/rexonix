package com.icedev.rexonix.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;

import com.icedev.rexonix.model.actor.Alien;
import com.icedev.rexonix.model.actor.Player;
import com.icedev.rexonix.model.common.Collider;
import com.icedev.rexonix.model.common.Level;
import com.icedev.rexonix.model.common.LevelProvider;
import com.icedev.rexonix.model.layer.Earth;
import com.icedev.rexonix.model.layer.FillHelper;
import com.icedev.rexonix.model.layer.Layer;
import com.icedev.rexonix.model.layer.Path;
import com.icedev.rexonix.model.utils.Serializer;

public class World extends AbstractModel {

	private static final long serialVersionUID = 1L;

	public static final String WORLD = "World";

	private int levelNumber = -1;

	private transient LevelProvider levelProvider;

	private boolean isFinished = false;

	private boolean isProcessed = false;

	private int life = 3;

	private transient List<Alien> aliens;

	public World(IModelContext context, LevelProvider levelProvider) {
		super(context, WORLD);
		setLevelProvider(levelProvider);
	}

	public void setLevelProvider(LevelProvider levelProvider) {
		this.levelProvider = levelProvider;
	}

	public int getLevelNumber() {
		return levelNumber;
	}

	public boolean isGaveOver() {
		return life == 0;
	}

	public int getLifeCount() {
		return life;
	}

	public void killed() {
		life--;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void stop() {
		this.isProcessed = false;
	}

	public void cont() {
		this.isProcessed = true;
	}

	public boolean isFinished() {
		if (isFinished)
			return true;
		isFinished = (levelNumber == levelProvider.getLevelCount() - 1)
				&& isFinishedLevel();
		return isFinished;
	}

	public boolean isFinishedLevel() {
		Level level = levelProvider.getLevel(levelNumber);
		return level.isFinished(this);
	}

	@Override
	public void removeChilds() {
		super.removeChilds();
		if (aliens != null)
			aliens = null;
	}

	public void nextLevel() {
		removeChilds();
		levelNumber++;
		Level level = levelProvider.getLevel(levelNumber);
		level.initializeWorld(this);
	}

	@Override
	public void down() {
		if (!isProcessed() || isGaveOver())
			return;
		super.down();
	}

	@Override
	public void up() {
		if (!isProcessed() || isGaveOver())
			return;
		super.up();
	}

	@Override
	public void left() {
		if (!isProcessed() || isGaveOver())
			return;
		super.left();
	}

	@Override
	public void right() {
		if (!isProcessed() || isGaveOver())
			return;
		super.right();
	}

	public Object serialize() {
		return Serializer.writeBytes(this);
	}

	public static World deserialize(Object buffer) {
		return (World) Serializer.readBytes((byte[]) buffer);
	}

	public void start() {
		removeChilds();
		nextLevel();
		cont();
	}

	public Earth getEarth() {
		return (Earth) getModel(Earth.EARTH);
	}

	public FillHelper getFillHelper() {
		return (FillHelper) getModel(FillHelper.FILL_HELPER);
	}

	public Path getPath() {
		return (Path) getModel(Path.PATH);
	}

	public List<Alien> getAliens() {
		if (aliens == null) {
			aliens = new ArrayList<Alien>();
			for (IModel child : getChilds()) {
				if (child instanceof Alien) {
					aliens.add((Alien) child);
				}
			}
		}
		return aliens;
	}

	public Player getPlayer() {
		return (Player) getModel(Player.PLAYER);
	}

	@Override
	public void calculate() {
		if (!isProcessed())
			return;

		if (isGaveOver())
			return;

		if (isFinishedLevel()) {
			if (isFinished()) {
				return;
			} else {
				nextLevel();
			}
		}

		Path path = getPath();
		Earth earth = getEarth();
		Player player = getPlayer();
		IModelContext context = getContext();
		List<Alien> aliens = getAliens();

		int[][] pathCells = path.getCells();
		int[][] earthCells = earth.getCells();

		// check for intersections and draw path
		if (path.isActive()) {
			boolean isMoved = false;
			for (int i = player.getPrevX(); i < player.getPrevX()
					+ player.getPrevWidth(); i++) {
				for (int j = player.getPrevY(); j < player.getPrevY()
						+ player.getPrevHeight(); j++) {
					if ((i < player.getX()
							|| i >= player.getX() + player.getWidth()
							|| j < player.getY() || j >= player.getY()
							+ player.getHeight())
							&& earthCells[i][j] == Layer.EMPTY) {
						pathCells[i][j] = Layer.FILLED;
						isMoved = true;
					}
				}
			}
			if (isMoved) {
				path.add(player.getCenterX(), player.getCenterY());
			}
		} else {
			if (earth.isIntersectEmpty(player.getX(), player.getY(),
					player.getWidth(), player.getHeight())) {
				path.activate(player.getPrevCenterX(), player.getPrevCenterY());
				player.saveLastActualState();
			}
		}

		if (path.isActive()) {
			if (isEatYourself())
				return;
			if (isEatedByAlien())
				return;
			if (isFilledContours())
				return;
		}

		// check alien for collision

		// Проверять вершины достаточно
		// TODO: Check
		for (Alien alien : aliens) {
			Collider.collideAct(alien, earthCells);
		}

		player.savePrevState();

		// update actors coordinates
		int newPlayerX = player.getX() + player.getSpeedX();
		if (newPlayerX >= 0
				&& newPlayerX + player.getWidth() <= context.getWidth())
			player.setX(newPlayerX);

		int newPlayerY = player.getY() + player.getSpeedY();
		if (newPlayerY >= 0
				&& newPlayerY + player.getHeight() <= context.getHeight())
			player.setY(newPlayerY);

		// update aliens coordinates
		for (Alien alien : aliens) {
			int newAlienX = alien.getX() + alien.getSpeedX();
			if (newAlienX >= 0
					&& newAlienX + alien.getWidth() <= context.getWidth())
				alien.setX(newAlienX);

			int newAlienY = alien.getY() + alien.getSpeedY();
			if (newAlienY >= 0
					&& newAlienY + alien.getHeight() <= context.getHeight())
				alien.setY(newAlienY);
		}

	}

	@SuppressLint("UseSparseArrays")
	public void refreshMarkArea() {
		Earth earth = getEarth();
		FillHelper fillHelper = getFillHelper();
		int[][] obstaclesMap = earth.getCells();
		fillHelper.clean();
		int[][] markArea = fillHelper.getCells();
		Map<Integer, Set<Integer>> markers = new HashMap<Integer, Set<Integer>>();
		Set<Integer> unmarked = new HashSet<Integer>();
		int emptySpace = 0;
		int maxMarker = 10;
		int currentMarker = maxMarker;
		boolean inMarkerZone = false;
		for (int j = 0; j < markArea[0].length; j++) {
			for (int i = 0; i < markArea.length; i++) {
				if (obstaclesMap[i][j] == emptySpace) {
					if (inMarkerZone) {
						if (j > 0 && markArea[i][j - 1] != emptySpace
								&& currentMarker != markArea[i][j - 1]) {
							Set<Integer> assignedMarkersIn = markers
									.get(currentMarker);
							Set<Integer> assignedMarkersOut = markers
									.get(markArea[i][j - 1]);
							assignedMarkersIn.add(markArea[i][j - 1]);
							assignedMarkersOut.add(currentMarker);
						}
						markArea[i][j] = currentMarker;

						for (Alien alien : aliens) {
							if (!(alien.getX() > i
									|| alien.getX() + alien.getWidth() < i
									|| alien.getY() > j || alien.getY()
									+ alien.getHeight() < j)) {
								unmarked.add(currentMarker);
								break;
							}
						}
					} else {
						if (j > 0 && markArea[i][j - 1] != emptySpace) {
							currentMarker = markArea[i][j - 1];
						} else {
							maxMarker++;
							currentMarker = maxMarker;
							markers.put(currentMarker, new HashSet<Integer>());
						}
						markArea[i][j] = currentMarker;
						Set<Integer> assignedMarkers = markers
								.get(currentMarker);
						if (assignedMarkers == null) {
							assignedMarkers = new HashSet<Integer>();
							markers.put(currentMarker, assignedMarkers);
						}
						inMarkerZone = true;
						for (Alien alien : aliens) {
							if (!(alien.getX() > i
									|| alien.getX() + alien.getWidth() < i
									|| alien.getY() > j || alien.getY()
									+ alien.getHeight() < j)) {
								unmarked.add(currentMarker);
								break;
							}
						}

					}
				} else {
					if (inMarkerZone) {
						inMarkerZone = false;
					}
				}
			}
		}

		for (Integer unmarkedZone : unmarked) {
			for (Integer removeZone : getRemoveZones(markers, unmarkedZone)) {
				markers.remove(removeZone);
			}
		}

		for (int i = 0; i < obstaclesMap.length; i++) {
			for (int j = 0; j < obstaclesMap[i].length; j++) {
				if (markArea[i][j] != Layer.EMPTY
						&& markers.containsKey(markArea[i][j])) {
					obstaclesMap[i][j] = Layer.FILLED;
				}
			}
		}
	}

	public void recRemoveZones(Map<Integer, Set<Integer>> markers,
			Set<Integer> removeList, Integer zone) {
		Set<Integer> assignedMarkers = markers.get(zone);
		if (assignedMarkers != null) {
			for (Integer assignedMarker : assignedMarkers) {
				if (!removeList.contains(assignedMarker)) {
					removeList.add(assignedMarker);
					recRemoveZones(markers, removeList, assignedMarker);
				}
			}
		}
	}

	public Set<Integer> getRemoveZones(Map<Integer, Set<Integer>> markers,
			Integer zone) {
		Set<Integer> removeList = new HashSet<Integer>();
		removeList.add(zone);
		recRemoveZones(markers, removeList, zone);
		return removeList;
	}

	// пересечения проверять достаточно по четырем вершинам
	protected boolean isFilledContours() {
		Path path = getPath();
		Earth earth = getEarth();
		Player player = getPlayer();

		int[][] earthCells = earth.getCells();
		int[][] pathCells = path.getCells();

		player.expandSingle();

		for (int i = player.getX(); i < player.getX() + player.getWidth(); i++) {
			for (int j = player.getY(); j < player.getY() + player.getHeight(); j++) {
				if ((i < player.getLastActualX()
						|| i >= player.getLastActualX()
								+ player.getLastActualWidth()
						|| j < player.getLastActualY() || j >= player
						.getLastActualY() + player.getLastActualHeight())
						&& earthCells[i][j] == Layer.FILLED) {

					player.restoreBeforeExpand();

					for (int k = 0; k < pathCells.length; k++) {
						for (int l = 0; l < pathCells[k].length; l++) {
							if (pathCells[k][l] == Layer.FILLED) {
								earthCells[k][l] = Layer.FILLED;
							}
						}
					}

					for (int k = player.getX(); k < player.getX()
							+ player.getWidth(); k++) {
						for (int l = player.getY(); l < player.getY()
								+ player.getHeight(); l++) {
							earthCells[k][l] = Layer.FILLED;
						}
					}

					refreshMarkArea();

					player.savePrevState();
					player.stop();
					path.clean();
					path.deactivate();
					earth.updateAreaValue();
					earth.updated();
					return true;
				}
			}
		}
		player.restoreBeforeExpand();
		return false;
	}

	protected boolean isEatYourself() {
		Path path = getPath();
		Player player = getPlayer();
		int[][] pathCells = path.getCells();
		for (int i = player.getX(); i < player.getX() + player.getWidth(); i++) {
			for (int j = player.getY(); j < player.getY() + player.getHeight(); j++) {
				if (pathCells[i][j] != Layer.EMPTY) {
					player.restoreLastActualState();
					player.savePrevState();
					player.stop();
					path.clean();
					path.deactivate();
					killed();
					return true;
				}
			}
		}
		return false;
	}

	public boolean isEatedByAlien() {
		Path path = getPath();
		Player player = getPlayer();
		int[][] pathCells = path.getCells();
		for (Alien alien : aliens) {
			// проверять по вершинам!
			for (int i = alien.getX(); i < alien.getX() + alien.getWidth(); i++) {
				for (int j = alien.getY(); j < alien.getY() + alien.getHeight(); j++) {
					if (pathCells[i][j] != Layer.EMPTY
							|| !(player.getX() > i
									|| player.getX() + player.getWidth() < i
									|| player.getY() > j || player.getY()
									+ player.getHeight() < j)) {
						player.restoreLastActualState();
						player.savePrevState();
						player.stop();
						path.clean();
						path.deactivate();
						killed();
						return true;
					}
				}
			}
		}
		return false;
	}

}
