package com.icedev.rexonix.view;

import org.andengine.entity.Entity;
import org.andengine.ui.activity.BaseGameActivity;

import com.icedev.rexonix.model.IModel;

public abstract class AbstractView extends Entity {

	private IModel model;

	private BaseGameActivity activity;

	public AbstractView(BaseGameActivity activity, IModel model) {
		this.model = model;
		this.activity = activity;
	}

	public IModel getModel() {
		return model;
	}

	public BaseGameActivity getActivity() {
		return activity;
	}

	public void update() {
	}

}
