package com.icedev.rexonix.model.common;

import com.icedev.rexonix.model.Compas;
import com.icedev.rexonix.model.actor.Actor;
import com.icedev.rexonix.model.layer.Layer;

public class Collider {

	
	public static void collideAct(Actor act, int[][] obstacles) {
		int speedX = act.getSpeedX();
		int speedY = act.getSpeedY();
		int collideDir = Collider.getCollideSide(act, obstacles, speedX > 0, speedY > 0);
		if(collideDir == Compas.UNKNOWN)
			return;
		if(collideDir == Compas.LEFT_UP || collideDir == Compas.RIGHT_DOWN || collideDir == Compas.UP_RIGHT || collideDir == Compas.DOWN_LEFT) {
			act.setSpeedX(-speedX);
			act.setSpeedY(-speedY);
		} else if(collideDir == Compas.LEFT || collideDir == Compas.RIGHT) {
			act.setSpeedX(-speedX);
		} else {
			act.setSpeedY(-speedY);
		}
	}

	
	/**
	 * 
	 * Определяет сторону столкновения Actor с точками на области cells
	 * 
	 * @param actor
	 * @param cells
	 * @return
	 */
	public static int getCollideSide(Actor actor, int[][] cells, boolean dirX,
			boolean dirY) {
		int actorX = actor.getX();
		int actorY = actor.getY();
		int endActorX = actorX + actor.getWidth() - 1;
		int endActorY = actorY + actor.getHeight() - 1;

		if (dirX) {
			if (dirY) {
				int countH = 0;
				for (int i = actorX; i <= endActorX; i++) {
					if (cells[i][endActorY] != Layer.EMPTY) {
						countH++;
					}
				}
				int countV = 0;
				for (int i = actorY; i <= endActorY; i++) {
					if (cells[endActorX][i] != Layer.EMPTY) {
						countV++;
					}
				}
				if (countH != 0 && countH == countV)
					return Compas.RIGHT_DOWN;
				if (countH > countV)
					return Compas.DOWN;
				if (countV > countH)
					return Compas.RIGHT;
			} else {
				int countH = 0;
				for (int i = actorX; i <= endActorX; i++) {
					if (cells[i][actorY] != Layer.EMPTY) {
						countH++;
					}
				}
				int countV = 0;
				for (int i = actorY; i <= endActorY; i++) {
					if (cells[endActorX][i] != Layer.EMPTY) {
						countV++;
					}
				}
				if (countH != 0 && countH == countV)
					return Compas.UP_RIGHT;
				if (countH > countV)
					return Compas.UP;
				if (countV > countH)
					return Compas.RIGHT;
			}
		} else {
			if (dirY) {
				int countH = 0;
				for (int i = actorX; i <= endActorX; i++) {
					if (cells[i][endActorY] != Layer.EMPTY) {
						countH++;
					}
				}
				int countV = 0;
				for (int i = actorY; i <= endActorY; i++) {
					if (cells[actorX][i] != Layer.EMPTY) {
						countV++;
					}
				}
				if (countH != 0 && countH == countV)
					return Compas.DOWN_LEFT;
				if (countH > countV)
					return Compas.DOWN;
				if (countV > countH)
					return Compas.LEFT;

			} else {
				int countH = 0;
				for (int i = actorX; i <= endActorX; i++) {
					if (cells[i][actorY] != Layer.EMPTY) {
						countH++;
					}
				}
				int countV = 0;
				for (int i = actorY; i <= endActorY; i++) {
					if (cells[actorX][i] != Layer.EMPTY) {
						countV++;
					}
				}
				if (countH != 0 && countH == countV)
					return Compas.LEFT_UP;
				if (countH > countV)
					return Compas.UP;
				if (countV > countH)
					return Compas.LEFT;
			}
		}
		return -1;
	}

}
