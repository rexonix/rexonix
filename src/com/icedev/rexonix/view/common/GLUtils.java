package com.icedev.rexonix.view.common;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import android.opengl.GLES20;

public class GLUtils {

	private static final int redOffset, greenOffset, blueOffset, alphaOffset;

	static {
		boolean big = ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN;
		redOffset = big ? 24 : 0;
		greenOffset = big ? 16 : 8;
		blueOffset = big ? 8 : 16;
		alphaOffset = big ? 0 : 24;
	}

	public static int packInt(int r, int g, int b, int a) {
		r = (r & 0xff) << redOffset;
		g = (g & 0xff) << greenOffset;
		b = (b & 0xff) << blueOffset;
		a = (a & 0xff) << alphaOffset;
		return r | g | b | a;
	}

	public static IntBuffer createIntBuffer(int capacity) {
		return ByteBuffer.allocateDirect(capacity << 2)
				.order(ByteOrder.nativeOrder()).order(ByteOrder.nativeOrder())
				.asIntBuffer();
	}

	public static IntBuffer wrapAsIntBuffer(int[] array) {
		IntBuffer intBuffer = createIntBuffer(array.length);
		intBuffer.put(array);
		intBuffer.flip();
		return intBuffer;
	}

	public static int[] convertRGBATextureToLinearIntArray(int[][][] texture) {
		int[] textureArray = new int[texture.length * texture[0].length];
		for (int j = 0; j < texture[0].length; j++) {
			for (int i = 0; i < texture.length; i++) {
				textureArray[j * texture.length + i] = packInt(
						texture[i][j][0], texture[i][j][1], texture[i][j][2],
						texture[i][j][3]);
			}
		}
		return textureArray;
	}

	public static int generateTextureFromRGBAArray(int[] texture, int width,
			int height) {
		IntBuffer intBuffer = wrapAsIntBuffer(texture);
		int textureIds[] = new int[1];
		GLES20.glGenTextures(1, textureIds, 0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);

		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width,
				height, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, intBuffer);
		GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);

		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				GLES20.GL_CLAMP_TO_EDGE);

		return textureIds[0];
	}

	public static int generateTexture(int[][][] texture) {
		return generateTextureFromRGBAArray(
				convertRGBATextureToLinearIntArray(texture), texture.length,
				texture[0].length);
	}

}
