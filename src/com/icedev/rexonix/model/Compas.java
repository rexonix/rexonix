package com.icedev.rexonix.model;

public class Compas {

	public final static int UNKNOWN = -1;
	
	public final static int UP = 1;

	public final static int UP_RIGHT = 2;

	public final static int RIGHT = 3;

	public final static int RIGHT_DOWN = 4;

	public final static int DOWN = 5;

	public final static int DOWN_LEFT = 6;

	public final static int LEFT = 7;

	public final static int LEFT_UP = 8;

}
